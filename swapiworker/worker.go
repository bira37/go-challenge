package swapiworker

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/bira37/go-challenge/pkg/env"
	"gitlab.com/bira37/go-challenge/pkg/mongodb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SimplePlanetInfo struct {
	Id       primitive.ObjectID `bson:"_id"`
	Name     string             `bson:"name"`
	LastEdit time.Time          `bson:"lastEdit"`
	Movies   int                `bson:"movies"`
}

// ReadPlanetsFromApi reads star wars planet infos from SWAPI, formats and saves into MongoDB
func ReadPlanetsFromApi() error {
	url := "https://swapi.dev/api/planets"

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}

	for currentPage := 1; ; currentPage++ {
		fmt.Printf("Reading page %v\n", currentPage)
		res, err := client.Get(fmt.Sprintf("%v?page=%v", url, currentPage))

		if err != nil {
			return err
		}

		if res.StatusCode != 200 {
			break
		}

		var obj map[string]interface{}

		byteRes, err := ioutil.ReadAll(res.Body)

		if err != nil {
			return err
		}

		err = json.Unmarshal(byteRes, &obj)

		if err != nil {
			return err
		}

		planetList := obj["results"].([]interface{})
		planets := make([]SimplePlanetInfo, 0)

		for _, v := range planetList {
			planetInfo := v.(map[string]interface{})
			movieList := planetInfo["films"].([]interface{})
			editDate, err := time.Parse(time.RFC3339, planetInfo["edited"].(string))

			if err != nil {
				return err
			}

			planet := SimplePlanetInfo{
				Id:       primitive.NewObjectID(),
				Name:     planetInfo["name"].(string),
				LastEdit: editDate,
				Movies:   len(movieList),
			}

			planets = append(planets, planet)
		}

		err = SavePlanetInfo(planets)

		if err != nil {
			return err
		}
	}

	return nil
}

// SavePlanetInfo reads an array of planet information and inserts them in DB, or replace an older entry if the update date is newer
func SavePlanetInfo(planets []SimplePlanetInfo) error {
	connection := mongodb.Connect()
	collection := connection.Database(env.MustGetString("DATABASE_NAME")).Collection("PlanetInfos")
	planetCollection := connection.Database(env.MustGetString("DATABASE_NAME")).Collection("Planets")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	for _, v := range planets {
		cursor := collection.FindOne(ctx, bson.M{"name": v.Name})
		var planet SimplePlanetInfo

		err := cursor.Decode(&planet)

		var planetCountChanged bool

		if err == nil && planet.Movies != v.Movies { // data is older, replace
			planetCountChanged = true
			planet.Movies = v.Movies
			planet.LastEdit = v.LastEdit
			_ = collection.FindOneAndReplace(ctx, bson.M{"name": planet.Name}, planet)
		} else { // data does not exists, insert
			planetCountChanged = true
			planet = SimplePlanetInfo{
				Id:       primitive.NewObjectID(),
				Name:     v.Name,
				LastEdit: v.LastEdit,
				Movies:   v.Movies,
			}

			_, err := collection.InsertOne(ctx, planet)

			if err != nil {
				return err
			}
		}

		if planetCountChanged { // update existing document
			update := bson.M{"$set": bson.M{"movies": v.Movies}}

			_, err := planetCollection.UpdateOne(ctx, bson.M{"name": v.Name}, update)

			if err != nil {
				return err
			}
		}
	}
	return nil
}

// ReadPlanetsFromFile reads star wars planet infos from seed file, formats and saves into MongoDB
func ReadPlanetsFromFile(path string) error {
	f, err := ioutil.ReadFile(path)

	if err != nil {
		return err
	}

	planets := make([]SimplePlanetInfo, 0)

	err = json.Unmarshal(f, &planets)

	if err != nil {
		return err
	}

	err = SavePlanetInfo(planets)

	if err != nil {
		return err
	}

	return nil
}
