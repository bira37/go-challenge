package util

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/bira37/go-challenge/api/errs"
)

type TestObject struct {
	Number int `json:"number" validate:"required"`
}

func TestParseBody(t *testing.T) {
	assert := assert.New(t)

	tests := []struct {
		body    string
		obj     TestObject
		errcode int
	}{
		{
			body:    `{"number": 42}`,
			obj:     TestObject{Number: 42},
			errcode: 0,
		},
		{
			body:    `{"number": "forty-two"}`,
			errcode: 400,
		},
		{
			body:    `{}`,
			errcode: 400,
		},
	}

	for _, tc := range tests {
		e := echo.New()

		body := bytes.NewBufferString(tc.body)

		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		res := httptest.NewRecorder()
		ctx := e.NewContext(req, res)

		var parsed TestObject

		err := ParseBody(ctx, &parsed)

		if tc.errcode == 0 {
			assert.NoError(err)

			assert.Equal(tc.obj, parsed)
		} else {
			httperr := err.(*errs.HttpError)

			assert.Equal(httperr.StatusCode, tc.errcode)
		}
	}
}
