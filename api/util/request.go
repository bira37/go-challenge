package util

import (
	"encoding/json"
	"fmt"

	"gitlab.com/bira37/go-challenge/api/errs"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

func ParseBody(e echo.Context, obj interface{}) error {
	if err := e.Bind(obj); err != nil {
		var message string

		echoErr, _ := err.(*echo.HTTPError)

		switch e := echoErr.Internal.(type) {
		case *json.UnmarshalTypeError:
			message = formatUnmarshallTypeError(e)
		default:
			message = err.Error()
		}
		return errs.NewHttpError(message, 400)
	}

	bodyValidator := validator.New()

	if err := bodyValidator.Struct(obj); err != nil {
		verr, _ := err.(validator.ValidationErrors)
		return errs.NewHttpError(formatValidationErrors(verr), 400)
	}

	return nil
}

func formatUnmarshallTypeError(err *json.UnmarshalTypeError) string {
	return fmt.Sprintf("'%s' should have type '%s', but has type '%s'.", err.Field, err.Type, err.Value)
}

func formatValidationErrors(err validator.ValidationErrors) string {
	var message string
	if len(err) > 1 {
		message += "Several errors occurred:"
	}
	for _, elem := range err {
		if elem.ActualTag() == "required" {
			message += fmt.Sprintf("\n'%s' is required.", elem.Field())
		} else {
			message += fmt.Sprintf("\nValue for '%s' is invalid.", elem.Field())
		}
	}
	return message
}
