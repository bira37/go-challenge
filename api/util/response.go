package util

import (
	"gitlab.com/bira37/go-challenge/api/errs"

	"github.com/labstack/echo/v4"
)

type ErrorResponse struct {
	Message string `json:"message"`
}

func SetResponse(e echo.Context, obj interface{}, err error) error {
	if err == nil {
		return setSuccess(e, obj)
	} else {
		return setError(e, err)
	}
}

func setSuccess(e echo.Context, obj interface{}) error {
	return e.JSON(200, obj)
}

func setError(e echo.Context, err error) error {
	httpError, parseOk := err.(*errs.HttpError)

	if !parseOk {
		httpError = errs.NewHttpError(err.Error(), 500)
	}

	return echo.NewHTTPError(httpError.StatusCode, httpError.Message)
}
