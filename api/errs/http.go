package errs

import "fmt"

type HttpError struct {
	Message    string
	StatusCode int
}

func NewHttpError(message string, status int) *HttpError {
	return &HttpError{
		Message:    message,
		StatusCode: status,
	}
}

func (err *HttpError) Error() string {
	return fmt.Sprintf("(%d): %s", err.StatusCode, err.Message)
}
