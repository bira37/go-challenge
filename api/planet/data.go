package planet

type InsertPlanetRequest struct {
	Name    string `json:"name" validate:"required"`
	Climate string `json:"climate" validate:"required"`
	Terrain string `json:"terrain" validate:"required"`
}

type PlanetResponse struct {
	InsertPlanetRequest
	Id     string `json:"id"`
	Movies int    `json:"movies"`
}

type DeletePlanetResponse struct {
	Id string `json:"id"`
}

type ListPlanetResponse struct {
	Page   int64            `json:"page"`
	Result []PlanetResponse `json:"result"`
}
