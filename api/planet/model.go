package planet

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Model struct {
	// Base
	Id        primitive.ObjectID `bson:"_id"`
	CreatedAt time.Time          `bson:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at"`

	// Data
	Name    string `bson:"name"`
	Climate string `bson:"climate"`
	Terrain string `bson:"terrain"`
	Movies  int    `bson:"movies"`
}

type PlanetInfoModel struct {
	Id       primitive.ObjectID `bson:"_id"`
	Name     string             `bson:"name"`
	LastEdit time.Time          `bson:"lastEdit"`
	Movies   int                `bson:"movies"`
}
