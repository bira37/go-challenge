package planet

import (
	"context"
	"time"

	"gitlab.com/bira37/go-challenge/pkg/env"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Store interface {
	InsertPlanet(planet Model) (Model, error)
	GetPlanet(oid primitive.ObjectID) (Model, error)
	GetPlanetByName(name string) (Model, error)
	DeletePlanet(oid primitive.ObjectID) error
	ListPlanet(page int64) ([]Model, error)
}

type store struct {
	Collection           *mongo.Collection
	PlanetInfoCollection *mongo.Collection
}

func NewStore(db *mongo.Database) *store {
	return &store{Collection: db.Collection("Planets"), PlanetInfoCollection: db.Collection("PlanetInfos")}
}

func (s *store) InsertPlanet(planet Model) (Model, error) {
	planet.CreatedAt = time.Now().UTC()
	planet.UpdatedAt = time.Now().UTC()
	planet.Id = primitive.NewObjectID()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cursor := s.PlanetInfoCollection.FindOne(ctx, bson.M{"name": planet.Name})

	var info PlanetInfoModel

	err := cursor.Decode(&info)

	if err != nil {
		planet.Movies = 0
	} else {
		planet.Movies = info.Movies
	}

	result, err := s.Collection.InsertOne(ctx, planet)

	if err != nil {
		return Model{}, err
	}

	planet.Id = result.InsertedID.(primitive.ObjectID)

	return planet, nil
}

func (s *store) GetPlanet(oid primitive.ObjectID) (Model, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result := s.Collection.FindOne(ctx, bson.M{"_id": oid})

	var planet Model

	err := result.Decode(&planet)

	if err != nil {
		return Model{}, ErrPlanetNotFound
	}

	return planet, nil
}

func (s *store) GetPlanetByName(name string) (Model, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result := s.Collection.FindOne(ctx, bson.M{"name": name})

	var planet Model

	err := result.Decode(&planet)

	if err != nil {
		return Model{}, ErrPlanetNotFound
	}

	return planet, nil
}

func (s *store) DeletePlanet(oid primitive.ObjectID) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err := s.Collection.DeleteOne(ctx, bson.M{"_id": oid})

	if err != nil {
		return err
	}

	return nil
}

func (s *store) ListPlanet(page int64) ([]Model, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	limit := env.GetInt64("LIST_PAGE_SIZE", 20)

	cursor, err := s.Collection.Find(ctx, bson.M{}, options.Find().SetLimit(limit), options.Find().SetSkip(page*limit))

	if err != nil {
		return []Model{}, err
	}

	result := []Model{}

	err = cursor.All(ctx, &result)

	if err != nil {
		return []Model{}, err
	}

	return result, nil
}
