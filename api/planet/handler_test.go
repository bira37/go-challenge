package planet

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func MockInsertPlanetRequest(name, climate, terrain string) InsertPlanetRequest {
	return InsertPlanetRequest{
		Name:    name,
		Climate: climate,
		Terrain: terrain,
	}
}

func MockModel(id primitive.ObjectID, name, climate, terrain string, movies int, now time.Time) Model {
	return Model{
		Id:        id,
		CreatedAt: now,
		UpdatedAt: now,
		Name:      name,
		Climate:   climate,
		Terrain:   terrain,
		Movies:    movies,
	}
}

func MockPlanetResponse(req InsertPlanetRequest, id string, movies int) PlanetResponse {
	return PlanetResponse{
		InsertPlanetRequest: req,
		Id:                  id,
		Movies:              movies,
	}
}

func MockListPlanetResponse(page int64, planets []PlanetResponse) ListPlanetResponse {
	return ListPlanetResponse{
		Page:   page,
		Result: planets,
	}
}

func TestInsertPlanetHandler(t *testing.T) {
	assert := assert.New(t)

	name := "Planet"
	climate := "Arid"
	terrain := "Desert"
	id := primitive.NewObjectID()
	now := time.Now().UTC()
	movies := 3

	tests := []struct {
		data              InsertPlanetRequest
		storeInsert       Model
		storeInsertErr    error
		storeGetByNameErr error
		response          PlanetResponse
		statusCode        int
	}{
		{
			data:              MockInsertPlanetRequest(name, climate, terrain),
			storeInsert:       MockModel(id, name, climate, terrain, movies, now),
			storeGetByNameErr: ErrPlanetNotFound,
			response:          MockPlanetResponse(MockInsertPlanetRequest(name, climate, terrain), id.Hex(), movies),
			statusCode:        200,
		},
		{
			data:       MockInsertPlanetRequest(name, climate, terrain),
			statusCode: 409,
		},
		{
			data:              MockInsertPlanetRequest(name, climate, terrain),
			storeGetByNameErr: ErrPlanetNotFound,
			storeInsertErr:    errors.New("Internal Error"),
			statusCode:        500,
		},
		{
			statusCode: 400,
		},
	}

	for _, tc := range tests {
		e := echo.New()

		body, err := json.Marshal(tc.data)

		assert.NoError(err)

		req := httptest.NewRequest(http.MethodPost, "/planet", bytes.NewBuffer(body))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		res := httptest.NewRecorder()
		ctx := e.NewContext(req, res)
		store := NewMockStore()

		store.GetByNameError = tc.storeGetByNameErr
		store.InsertError = tc.storeInsertErr
		store.InsertResponse = tc.storeInsert

		hand := handler{Store: store}

		httperr := hand.InsertPlanet(ctx)

		if tc.statusCode == 200 {
			assert.Equal(200, res.Result().StatusCode)

			var response PlanetResponse

			err = json.NewDecoder(res.Body).Decode(&response)

			assert.NoError(err)

			assert.Equal(response, tc.response)
		} else {
			echoerr := httperr.(*echo.HTTPError)

			assert.Equal(echoerr.Code, tc.statusCode)
		}
	}
}

func TestGetPlanetHandler(t *testing.T) {
	assert := assert.New(t)

	name := "Planet"
	climate := "Arid"
	terrain := "Desert"
	id := primitive.NewObjectID()
	now := time.Now().UTC()
	movies := 3
	searchId := id.Hex()

	tests := []struct {
		storeGetErr error
		storeGet    Model
		response    PlanetResponse
		statusCode  int
		searchId    string
	}{
		{
			storeGet:   MockModel(id, name, climate, terrain, movies, now),
			response:   MockPlanetResponse(MockInsertPlanetRequest(name, climate, terrain), id.Hex(), movies),
			statusCode: 200,
			searchId:   searchId,
		},
		{
			storeGetErr: ErrPlanetNotFound,
			statusCode:  404,
			searchId:    primitive.NewObjectID().Hex(),
		},
		{
			statusCode: 400,
			searchId:   "invalid id",
		},
	}

	for _, tc := range tests {
		e := echo.New()

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		res := httptest.NewRecorder()
		ctx := e.NewContext(req, res)
		ctx.SetPath("/planet/:id")
		ctx.SetParamNames("id")
		ctx.SetParamValues(tc.searchId)

		store := NewMockStore()

		store.GetError = tc.storeGetErr
		store.GetResponse = tc.storeGet

		hand := handler{Store: store}

		httperr := hand.GetPlanet(ctx)

		if tc.statusCode == 200 {
			assert.Equal(200, res.Result().StatusCode)

			var response PlanetResponse

			err := json.NewDecoder(res.Body).Decode(&response)

			assert.NoError(err)

			assert.Equal(response, tc.response)
		} else {
			echoerr := httperr.(*echo.HTTPError)

			assert.Equal(echoerr.Code, tc.statusCode)
		}
	}
}

func TestGetPlanetByNameHandler(t *testing.T) {
	assert := assert.New(t)

	name := "Planet"
	climate := "Arid"
	terrain := "Desert"
	id := primitive.NewObjectID()
	now := time.Now().UTC()
	movies := 3
	searchName := name

	tests := []struct {
		storeGetByNameErr error
		storeGetByName    Model
		response          PlanetResponse
		statusCode        int
		searchName        string
	}{
		{
			storeGetByName: MockModel(id, name, climate, terrain, movies, now),
			response:       MockPlanetResponse(MockInsertPlanetRequest(name, climate, terrain), id.Hex(), movies),
			statusCode:     200,
			searchName:     searchName,
		},
		{
			storeGetByNameErr: ErrPlanetNotFound,
			statusCode:        404,
			searchName:        "another planet",
		},
	}

	for _, tc := range tests {
		e := echo.New()

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		res := httptest.NewRecorder()
		ctx := e.NewContext(req, res)
		ctx.SetPath("/planet/name/:name")
		ctx.SetParamNames("name")
		ctx.SetParamValues(tc.searchName)

		store := NewMockStore()

		store.GetByNameError = tc.storeGetByNameErr
		store.GetByNameResponse = tc.storeGetByName

		hand := handler{Store: store}

		httperr := hand.GetPlanetByName(ctx)

		if tc.statusCode == 200 {
			assert.Equal(200, res.Result().StatusCode)

			var response PlanetResponse

			err := json.NewDecoder(res.Body).Decode(&response)

			assert.NoError(err)

			assert.Equal(response, tc.response)
		} else {
			echoerr := httperr.(*echo.HTTPError)

			assert.Equal(echoerr.Code, tc.statusCode)
		}
	}
}

func TestGetDeletePlanetHandler(t *testing.T) {
	assert := assert.New(t)

	id := primitive.NewObjectID()
	searchId := id.Hex()

	tests := []struct {
		storeDeleteErr error
		response       DeletePlanetResponse
		statusCode     int
		searchId       string
	}{
		{
			response:   DeletePlanetResponse{Id: id.Hex()},
			statusCode: 200,
			searchId:   searchId,
		},
		{
			storeDeleteErr: errors.New("Internal error"),
			statusCode:     500,
			searchId:       searchId,
		},
		{
			statusCode: 400,
			searchId:   "invalid id",
		},
	}

	for _, tc := range tests {
		e := echo.New()

		req := httptest.NewRequest(http.MethodDelete, "/", nil)
		res := httptest.NewRecorder()
		ctx := e.NewContext(req, res)
		ctx.SetPath("/planet/:id")
		ctx.SetParamNames("id")
		ctx.SetParamValues(tc.searchId)

		store := NewMockStore()

		store.DeleteError = tc.storeDeleteErr

		hand := handler{Store: store}

		httperr := hand.DeletePlanet(ctx)

		if tc.statusCode == 200 {
			assert.Equal(200, res.Result().StatusCode)

			var response DeletePlanetResponse

			err := json.NewDecoder(res.Body).Decode(&response)

			assert.NoError(err)

			assert.Equal(response, tc.response)
		} else {
			echoerr := httperr.(*echo.HTTPError)

			assert.Equal(echoerr.Code, tc.statusCode)
		}
	}
}

func TestListPlanetHandler(t *testing.T) {
	assert := assert.New(t)

	name := "Planet"
	climate := "Arid"
	terrain := "Desert"
	id := primitive.NewObjectID()
	now := time.Now().UTC()
	movies := 3
	page := int64(1)

	tests := []struct {
		storeListErr  error
		storeList     []Model
		response      ListPlanetResponse
		statusCode    int
		page          int64
		shouldAddPage bool
	}{
		{
			response:      MockListPlanetResponse(page, []PlanetResponse{MockPlanetResponse(MockInsertPlanetRequest(name, climate, terrain), id.Hex(), movies)}),
			storeList:     []Model{MockModel(id, name, climate, terrain, movies, now)},
			statusCode:    200,
			page:          page,
			shouldAddPage: true,
		},
		{
			response:      MockListPlanetResponse(0, []PlanetResponse{MockPlanetResponse(MockInsertPlanetRequest(name, climate, terrain), id.Hex(), movies)}),
			storeList:     []Model{MockModel(id, name, climate, terrain, movies, now)},
			statusCode:    200,
			page:          0,
			shouldAddPage: false,
		},
		{
			statusCode:    400,
			page:          -1,
			shouldAddPage: true,
		},
		{
			statusCode:    500,
			storeListErr:  errors.New("Internal error"),
			shouldAddPage: false,
		},
	}

	for _, tc := range tests {
		e := echo.New()

		qry := make(url.Values)

		if tc.shouldAddPage {
			qry.Set("page", strconv.FormatInt(tc.page, 10))
		}

		req := httptest.NewRequest(http.MethodGet, "/planet?"+qry.Encode(), nil)
		res := httptest.NewRecorder()
		ctx := e.NewContext(req, res)

		store := NewMockStore()

		store.ListError = tc.storeListErr
		store.ListResponse = tc.storeList

		hand := handler{Store: store}

		httperr := hand.ListPlanet(ctx)

		if tc.statusCode == 200 {
			assert.Equal(200, res.Result().StatusCode)

			var response ListPlanetResponse

			err := json.NewDecoder(res.Body).Decode(&response)

			assert.NoError(err)

			assert.Equal(response, tc.response)
			assert.Equal(response.Page, tc.page)
		} else {
			echoerr := httperr.(*echo.HTTPError)

			assert.Equal(echoerr.Code, tc.statusCode)
		}
	}
}
