package planet

import (
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/bira37/go-challenge/api/util"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Handler interface {
	InsertPlanet(echo.Context) error
	GetPlanet(echo.Context) error
	GetByName(echo.Context) error
	DeletePlanet(echo.Context) error
	ListPlanet(e echo.Context) error
}

type handler struct {
	Store Store
}

func NewHandler(store Store) *handler {
	return &handler{Store: store}
}

func (h *handler) InsertPlanet(e echo.Context) error {
	var req InsertPlanetRequest
	var res PlanetResponse

	err := util.ParseBody(e, &req)

	if err != nil {
		return util.SetResponse(e, res, err)
	}

	_, err = h.Store.GetPlanetByName(req.Name)

	if err == nil {
		return util.SetResponse(e, res, ErrPlanetNameConflict)
	}

	planet, err := h.Store.InsertPlanet(Model{Name: req.Name, Climate: req.Climate, Terrain: req.Terrain})

	if err != nil {
		return util.SetResponse(e, res, err)
	}

	res = PlanetResponse{
		Id:                  planet.Id.Hex(),
		InsertPlanetRequest: req,
		Movies:              planet.Movies,
	}

	return util.SetResponse(e, res, nil)
}

func (h *handler) GetPlanet(e echo.Context) error {
	var res PlanetResponse

	id := e.Param("id")

	oid, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return util.SetResponse(e, res, ErrInvalidPlanetId)
	}

	planet, err := h.Store.GetPlanet(oid)

	if err != nil {
		return util.SetResponse(e, res, err)
	}

	res = PlanetResponse{
		Id: planet.Id.Hex(),
		InsertPlanetRequest: InsertPlanetRequest{
			Name:    planet.Name,
			Climate: planet.Climate,
			Terrain: planet.Terrain,
		},
		Movies: planet.Movies,
	}

	return util.SetResponse(e, res, nil)
}

func (h *handler) GetPlanetByName(e echo.Context) error {
	var res PlanetResponse

	name := e.Param("name")

	planet, err := h.Store.GetPlanetByName(name)

	if err != nil {
		return util.SetResponse(e, res, err)
	}

	res = PlanetResponse{
		Id:     planet.Id.Hex(),
		Movies: planet.Movies,
		InsertPlanetRequest: InsertPlanetRequest{
			Name:    planet.Name,
			Climate: planet.Climate,
			Terrain: planet.Terrain,
		},
	}

	return util.SetResponse(e, res, nil)
}

func (h *handler) DeletePlanet(e echo.Context) error {
	var res DeletePlanetResponse
	id := e.Param("id")

	oid, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return util.SetResponse(e, res, ErrInvalidPlanetId)
	}

	err = h.Store.DeletePlanet(oid)

	if err != nil {
		return util.SetResponse(e, res, err)
	}

	res = DeletePlanetResponse{
		Id: id,
	}

	return util.SetResponse(e, res, nil)
}

func (h *handler) ListPlanet(e echo.Context) error {
	var res ListPlanetResponse

	pageStr := e.QueryParam("page")

	page := int64(0)
	var err error

	if len(pageStr) > 0 {
		page, err = strconv.ParseInt(e.QueryParam("page"), 10, 64)

		if err != nil || page < 0 {
			return util.SetResponse(e, res, ErrInvalidPageNumberFormat)
		}
	}

	planets, err := h.Store.ListPlanet(page)

	if err != nil {
		return util.SetResponse(e, res, err)
	}

	res.Page = page
	res.Result = make([]PlanetResponse, 0)

	for _, planet := range planets {
		res.Result = append(res.Result, PlanetResponse{
			InsertPlanetRequest: InsertPlanetRequest{
				Name:    planet.Name,
				Climate: planet.Climate,
				Terrain: planet.Terrain,
			},
			Id:     planet.Id.Hex(),
			Movies: planet.Movies,
		})
	}

	return util.SetResponse(e, res, nil)
}
