package planet

import "gitlab.com/bira37/go-challenge/api/errs"

var ErrInvalidPlanetId = errs.NewHttpError("Invalid format for planet id", 400)

var ErrPlanetNotFound = errs.NewHttpError("Planet not found", 404)

var ErrPlanetNameConflict = errs.NewHttpError("Planet with same name already exists", 409)

var ErrInvalidPageNumberFormat = errs.NewHttpError("Page must be an non-negative integer", 400)
