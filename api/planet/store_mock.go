package planet

import "go.mongodb.org/mongo-driver/bson/primitive"

type MockStore struct {
	GetResponse       Model
	GetError          error
	InsertResponse    Model
	InsertError       error
	GetByNameResponse Model
	GetByNameError    error
	DeleteError       error
	ListError         error
	ListResponse      []Model
}

func NewMockStore() *MockStore {
	return &MockStore{}
}

func (m *MockStore) InsertPlanet(planet Model) (Model, error) {
	return m.InsertResponse, m.InsertError
}

func (m *MockStore) GetPlanet(oid primitive.ObjectID) (Model, error) {
	return m.GetResponse, m.GetError
}

func (m *MockStore) GetPlanetByName(name string) (Model, error) {
	return m.GetByNameResponse, m.GetByNameError
}

func (m *MockStore) DeletePlanet(oid primitive.ObjectID) error {
	return m.DeleteError
}

func (m *MockStore) ListPlanet(page int64) ([]Model, error) {
	return m.ListResponse, m.ListError
}
