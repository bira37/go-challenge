package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/bira37/go-challenge/api/planet"
	"gitlab.com/bira37/go-challenge/pkg/env"
	"gitlab.com/bira37/go-challenge/pkg/mongodb"
)

func Setup() *echo.Echo {
	e := echo.New()

	connection := mongodb.Connect()

	db := connection.Database(env.MustGetString("DATABASE_NAME"))

	// Stores
	PlanetStore := planet.NewStore(db)
	// Handlers
	PlanetHandler := planet.NewHandler(PlanetStore)

	// Health Check
	health := e.Group("/health")
	health.GET("", func(e echo.Context) error {
		return e.JSON(200, map[string]string{"health": "OK"})
	})

	// Endpoints
	planet := e.Group("/planet")
	planet.POST("", PlanetHandler.InsertPlanet)
	planet.GET("/name/:name", PlanetHandler.GetPlanetByName)
	planet.GET("/:id", PlanetHandler.GetPlanet)
	planet.GET("", PlanetHandler.ListPlanet)
	planet.DELETE("/:id", PlanetHandler.DeletePlanet)
	return e
}
