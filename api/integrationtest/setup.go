package integrationtest

import (
	"context"
	"log"
	"net/http/httptest"
	"time"

	"gitlab.com/bira37/go-challenge/api/server"
	"gitlab.com/bira37/go-challenge/pkg/mongodb"
	"go.mongodb.org/mongo-driver/mongo"
)

var Server *httptest.Server
var MongoClient *mongo.Client

func Setup() {
	Server = httptest.NewServer(server.Setup())
	MongoClient = mongodb.Connect()
}

func Teardown() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := MongoClient.Disconnect(ctx)

	if err != nil {
		log.Fatal(err)
	}

	Server.Close()
}
