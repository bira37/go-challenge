package integrationtest

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bira37/go-challenge/api/planet"
	"gitlab.com/bira37/go-challenge/pkg/env"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func AddMovieCountForPlanet(name string, count int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	collection := MongoClient.Database(env.MustGetString("DATABASE_NAME")).Collection("PlanetInfos")

	_, err := collection.InsertOne(ctx, planet.PlanetInfoModel{Name: name, LastEdit: time.Now().UTC(), Movies: count, Id: primitive.NewObjectID()})

	if err != nil {
		return err
	}

	return nil
}

func AddPlanetToDB(name string, climate string, terrain string, movieCount int) (planet.Model, error) {
	store := planet.NewStore(MongoClient.Database(env.MustGetString("DATABASE_NAME")))

	err := AddMovieCountForPlanet(name, movieCount)

	if err != nil {
		return planet.Model{}, err
	}

	pln, err := store.InsertPlanet(planet.Model{Name: name, Climate: climate, Terrain: terrain})

	if err != nil {
		return planet.Model{}, err
	}

	return pln, nil
}

func TestInsertPlanet(t *testing.T) {
	assert := assert.New(t)

	statusCode := 200
	data := planet.InsertPlanetRequest{
		Name:    "InsertPlanet",
		Climate: "Arid",
		Terrain: "Desert",
	}
	fakeMovieCount := 5

	err := AddMovieCountForPlanet(data.Name, fakeMovieCount)

	assert.NoError(err)

	body, err := json.Marshal(data)

	assert.NoError(err)

	res, err := http.Post(fmt.Sprintf("%s/planet", Server.URL), "application/json", bytes.NewBuffer(body))

	assert.NoError(err)

	assert.Equal(res.StatusCode, statusCode)

	var response planet.PlanetResponse

	err = json.NewDecoder(res.Body).Decode(&response)

	assert.NoError(err)

	if response.Name != data.Name ||
		response.Climate != data.Climate ||
		response.Terrain != data.Terrain ||
		response.Movies != fakeMovieCount {
		t.Errorf("expected equivalent of %v with movie count equal to %v, but got %v", data, fakeMovieCount, response)
	}
}

func TestGetPlanet(t *testing.T) {
	assert := assert.New(t)

	statusCode := 200

	name := "GetPlanet"
	climate := "Arid"
	terrain := "Desert"
	movieCount := 5

	pln, err := AddPlanetToDB(name, climate, terrain, movieCount)

	assert.NoError(err)

	res, err := http.Get(fmt.Sprintf("%s/planet/%s", Server.URL, pln.Id.Hex()))

	assert.NoError(err)

	if res.StatusCode != statusCode {
		t.Errorf("expected status %v, but got %v", statusCode, res.StatusCode)
	}

	var response planet.PlanetResponse

	err = json.NewDecoder(res.Body).Decode(&response)

	assert.NoError(err)

	if response.Name != name ||
		response.Climate != climate ||
		response.Terrain != terrain ||
		response.Movies != movieCount {
		t.Errorf("expected equivalent of {%v %v %v} with movie count equal to %v, but got %v", name, climate, terrain, movieCount, response)
	}
}

func TestGetPlanetNotFound(t *testing.T) {
	assert := assert.New(t)

	statusCode := 404

	res, err := http.Get(fmt.Sprintf("%s/planet/%s", Server.URL, primitive.NewObjectID().Hex()))

	assert.NoError(err)

	assert.Equal(statusCode, res.StatusCode)
}

func TestGetPlanetByName(t *testing.T) {
	assert := assert.New(t)

	statusCode := 200

	name := "GetPlanetByName"
	climate := "Arid"
	terrain := "Desert"
	movieCount := 5

	pln, err := AddPlanetToDB(name, climate, terrain, movieCount)

	assert.NoError(err)

	res, err := http.Get(fmt.Sprintf("%s/planet/name/%s", Server.URL, pln.Name))

	assert.NoError(err)

	if res.StatusCode != statusCode {
		t.Errorf("expected status %v, but got %v", statusCode, res.StatusCode)
	}

	var response planet.PlanetResponse

	err = json.NewDecoder(res.Body).Decode(&response)

	assert.NoError(err)

	if response.Name != name ||
		response.Climate != climate ||
		response.Terrain != terrain ||
		response.Movies != movieCount {
		t.Errorf("expected equivalent of {%v %v %v} with movie count equal to %v, but got %v", name, climate, terrain, movieCount, response)
	}
}

func TestDeletePlanet(t *testing.T) {
	assert := assert.New(t)

	statusCode := 200

	name := "GetPlanet"
	climate := "Arid"
	terrain := "Desert"
	movieCount := 5

	pln, err := AddPlanetToDB(name, climate, terrain, movieCount)

	assert.NoError(err)

	client := &http.Client{}

	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s/planet/%s", Server.URL, pln.Id.Hex()), nil)

	assert.NoError(err)

	res, err := client.Do(req)

	assert.NoError(err)

	if res.StatusCode != statusCode {
		t.Errorf("expected status %v, but got %v", statusCode, res.StatusCode)
	}

	var response planet.DeletePlanetResponse

	err = json.NewDecoder(res.Body).Decode(&response)

	assert.NoError(err)

	if response.Id != pln.Id.Hex() {
		t.Errorf("expected id %v, but got %v", pln.Id.Hex(), response.Id)
	}
}

func TestListPlanet(t *testing.T) {
	assert := assert.New(t)
	statusCode := 200

	name := "ListPlanet"
	climate := "Arid"
	terrain := "Desert"
	movieCount := 5

	pln, err := AddPlanetToDB(name, climate, terrain, movieCount)

	assert.NoError(err)

	res, err := http.Get(fmt.Sprintf("%s/planet", Server.URL))

	assert.NoError(err)

	if res.StatusCode != statusCode {
		t.Errorf("expected status %v, but got %v", statusCode, res.StatusCode)
	}

	var response planet.ListPlanetResponse

	err = json.NewDecoder(res.Body).Decode(&response)

	assert.NoError(err)

	found := false

	for _, item := range response.Result {
		if item.Id == pln.Id.Hex() &&
			item.Name == pln.Name &&
			item.Climate == pln.Climate &&
			item.Terrain == pln.Terrain &&
			item.Movies == pln.Movies {
			found = true
			break
		}
	}

	if !found {
		t.Errorf("expected planet %v not found in listing %v", pln, response.Result)
	}
}
