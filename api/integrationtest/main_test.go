package integrationtest

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	Setup()
	exitCode := m.Run()
	Teardown()
	os.Exit(exitCode)
}
