docker build -t swapiworker -f build/Dockerfile.worker .

docker run --rm --env-file .env -e PLANET_SEED_PATH=/app/data/planetseed.json --network go-challenge_app_network swapiworker /app/worker -mode $1