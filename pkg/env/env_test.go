package env

import (
	"os"
	"strconv"
	"testing"
)

func TestGetString(t *testing.T) {
	value := "value"
	def := "default"

	os.Setenv("GET_STRING", value)

	result := GetString("GET_STRING", def)

	if result != value {
		t.Errorf("expected %v, but got %v", value, result)
	}

	result = GetString("GET_STRING_DEFAULT", def)

	if result != def {
		t.Errorf("expected %v, but got %v", def, result)
	}
}

func TestMustGetString(t *testing.T) {
	env := "MUST_GET_STRING"
	value := "value"

	os.Setenv(env, value)

	result := MustGetString(env)

	if result != value {
		t.Errorf("expected %v, but got %v", value, result)
	}
}

func TestMustGetStringPanic(t *testing.T) {
	env := "MUST_GET_STRING_PANIC"

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("expected call to MustGetString to panic")
		}
	}()

	_ = MustGetString(env)
}

func TestGetInt64(t *testing.T) {
	value := int64(37)
	def := int64(42)
	overflowValue := "9323372036854775807"

	os.Setenv("GET_INT64", strconv.FormatInt(value, 10))

	result := GetInt64("GET_INT64", def)

	if result != value {
		t.Errorf("expected %v, but got %v", value, result)
	}

	os.Setenv("GET_INT64_DEFAULT", overflowValue)

	result = GetInt64("GET_INT64_DEFAULT", def)

	if result != def {
		t.Errorf("expected %v, but got %v", def, result)
	}
}

func TestMustGetInt64(t *testing.T) {
	env := "MUST_GET_INT64"
	value := int64(42)

	os.Setenv(env, strconv.FormatInt(value, 10))

	result := MustGetInt64(env)

	if result != value {
		t.Errorf("expected %v, but got %v", value, result)
	}
}

func TestMustGetInt64Panic(t *testing.T) {
	env := "MUST_GET_INT64_PANIC"
	os.Setenv(env, "string value")

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("expected call to MustGetInt64 to panic")
		}
	}()

	_ = MustGetInt64(env)
}
