package env

import (
	"fmt"
	"os"
	"strconv"
)

// GetString returns an environment variable as string or the default value given
func GetString(env string, def string) string {
	if val, ok := os.LookupEnv(env); ok {
		return val
	}
	return def
}

// MustGetString returns an environment variable as string or panics
func MustGetString(env string) string {
	if val, ok := os.LookupEnv(env); ok {
		return val
	}
	panic(fmt.Errorf("Variable %v not set.", env))
}

// GetInt returns an environment variable as Int or the default value given
func GetInt64(env string, def int64) int64 {
	val, err := strconv.ParseInt(GetString(env, strconv.FormatInt(def, 10)), 10, 64)
	if err != nil {
		return def
	}
	return int64(val)
}

// GetInt returns an environment variable as Int or panics
func MustGetInt64(env string) int64 {
	val, err := strconv.ParseInt(MustGetString(env), 10, 64)
	if err != nil {
		panic(err)
	}
	return int64(val)
}
