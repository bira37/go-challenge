package mongodb

import (
	"context"
	"time"

	"gitlab.com/bira37/go-challenge/pkg/env"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Connect() *mongo.Client {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(env.MustGetString("CONNECTION_STRING")))
	if err != nil {
		panic(err)
	}
	return client
}
