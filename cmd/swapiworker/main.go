package main

import (
	"flag"
	"fmt"

	"github.com/joho/godotenv"
	"gitlab.com/bira37/go-challenge/pkg/env"
	"gitlab.com/bira37/go-challenge/swapiworker"
)

func main() {
	_ = godotenv.Load()

	mode := flag.String("mode", "api", "Specifies wheter data will be read from file or api. Possible values: (api | file). For the file mode, data will be read from PLANET_SEED_PATH environment variable.")

	flag.Parse()

	var err error

	switch *mode {
	case "api":
		err = swapiworker.ReadPlanetsFromApi()
	case "file":
		err = swapiworker.ReadPlanetsFromFile(env.MustGetString("PLANET_SEED_PATH"))
	default:
		err = fmt.Errorf("Unexpected value for 'mode' flag")
	}

	if err != nil {
		panic(err)
	}

	fmt.Println("Finished without errors")
}
