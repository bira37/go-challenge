package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/bira37/go-challenge/api/server"
)

func main() {
	_ = godotenv.Load()

	e := server.Setup()

	e.Logger.Fatal(e.Start(":7000"))
}
