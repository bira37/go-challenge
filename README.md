# Go Challenge - Planet API

## Objetivo

O objetivo deste projeto é criar uma API que fornece funcionalidades para adicionar, remover, listar e buscar planetas do universo de Star Wars. Este documento será o guia de como inicializar e utilizar essa API.

## Requisitos

Para a execução, é necessário ter instalado:

- Docker (19.03.0+)
- Docker Compose (Compatível com Compose File 3.8)
- (Opcional, para execução dos testes localmente) Go 1.16
- (Opcional) [Ferramenta GoCov](https://github.com/axw/gocov) para geração e visualização de coverage consolidado

## Sobre o projeto

Este projeto possui dois executáveis: uma API que oferece as funcionalidades descritas acima, e um script executável que busca informações relacionadas à aparição de cada planeta nos filmes Star Wars. Este script utiliza dados da [SWAPI](https://swapi.dev/) para manter a API atualizada quanto à quantidade de aparições de cada planeta registrado. O guia iniciará apresentando como executar o projeto, seguido de informações sobre a API, e logo depois mostrará mais detalhes sobre o script. Por fim, serão apresentados os testes e informações extras sobre o projeto.

## Instruções

Para executar o projeto, é necessário inicialmente copiar o conteúdo do arquivo `.env.example` para um novo arquivo que deve ser nomeado `.env` no mesmo diretório. Este arquivo possui variáveis de ambiente que precisam existir para que o projeto funcione corretamente. Caso esteja utilizando o bash, basta executar o comando:

```
cp .env.example .env
```

É necessário também que as portas 7000 e 27017 do seu computador não estejam em uso. A porta 7000 será utilizada pela API, enquanto a porta 27017 será utilizada pelo MongoDB, banco de dados que foi escolhido para a criação do projeto.

Com os passos acima realizados, basta executar o comando

```
docker-compose up -d
```

E serão inicializados todos os serviços necessários para a execução do projeto. Executando o comando acima fará com que uma instância do MongoDB esteja disponível para uso, que a API escute requisições na porta 7000, e faz o script de busca requisitar dados da SWAPI e salvar dentro do banco. A API e o script de busca são construídos utilizando os arquivos `build/Dockerfile.api` e `build/Dockerfile.worker` respectivamente. Para interromper e remover todos os containers, pode-se executar o comando

```
docker-compose down -v
```

## API

A API foi construída utilizando [Echo](https://github.com/labstack/echo) e o [MongoDBDriver](https://github.com/mongodb/mongo-go-driver). Os dados inseridos por usuários se encontram na collection `Planets` do database `swapi` dentro do MongoDB.

Um ponto a ressaltar sobre a API é que ela opera independentemente da execução do script de busca. Mesmo que o planeta inputado não tenha sido encontrado na SWAPI, ele será inserido sem falhas. Esta decisão foi tomada para que possíveis planetas novos que ainda não tenham sido registrados na SWAPI possam também ser inseridos no sistema. É importante pontuar também que a contagem de aparições de um planeta em filmes é atualizada caso uma nova execução do script de busca seja feita e retorne um resultado diferente do que foi registrado anteriormente. Isto permite que a API possua sempre dados atualizados do universo Star Wars (dependendo apenas da periodicidade da execução do script).

A demonstração dos endpoints tomará como base a API executada no endereço http://localhost:7000. Os recursos acessíveis estão listados abaixo.

### Inserir novo planeta

```
POST http://localhost:7000/planet

Body: {
  "name": "Tatooine",
  "climate": "Arid",
  "terrain": "Desert"
}

Resposta: {
  "name": "Tatooine",
  "climate": "Arid",
  "terrain": "Desert",
  "id": "609e8338e9a703448546199a",
  "movies": 5
}

Exemplo de resposta de falha: {
  "message": "Planet with same name already exists"
}
```

### Buscar planeta

```
GET http://localhost:7000/planet/609e8338e9a703448546199a

Resposta: {
  "name": "Tatooine",
  "climate": "Arid",
  "terrain": "Desert",
  "id": "609e8338e9a703448546199a",
  "movies": 5
}

Exemplo de resposta de falha: {
  "message": "Planet not found"
}
```

### Buscar planeta pelo nome

```
GET http://localhost:7000/planet/name/Tatooine

Resposta: {
  "name": "Tatooine",
  "climate": "Arid",
  "terrain": "Desert",
  "id": "609e8338e9a703448546199a",
  "movies": 5
}

Exemplo de resposta de falha: {
  "message": "Planet not found"
}
```

### Listar planetas

A paginação da API inicia em 0, como demonstrado abaixo.

```
GET http://localhost:7000/planet

Resposta: {
  "page": 0,
  "result": [
    {
      "name": "Tatooine",
      "climate": "Arid",
      "terrain": "Desert",
      "id": "609e8338e9a703448546199a",
      "movies": 5
    }
  ]
}

GET http://localhost:7000/planet?page=0

Resposta: {
  "page": 0,
  "result": [
    {
      "name": "Tatooine",
      "climate": "Arid",
      "terrain": "Desert",
      "id": "609e8338e9a703448546199a",
      "movies": 5
    }
  ]
}

GET http://localhost:7000/planet?page=1

Resposta: {
  "page": 1,
  "result": []
}
```

### Deletar planeta

A deleção funciona de forma idempotente: independentemente da existência do registro, será retornado uma mensagem de sucesso (exceto em casos de erros inesperados).

```
DELETE http://localhost:7000/planet/609e8338e9a703448546199a

Resposta: {
  "id": "609e8338e9a703448546199a"
}
```

## Script de busca

As funcionalidades existentes do script de busca podem ser encontradas no arquivo `swapiworker/worker.go`. Este script realiza a busca das informações da SWAPI, formata, e extrai os dados relevantes para o projeto. Ao executar o projeto, ele realiza a busca uma única vez. Porém, ele pode ser periodicamente executado com o comando `docker-compose start worker` sempre que for necessário enquanto o banco estiver em execução. Os dados obtidos da SWAPI ficam salvos na collection `PlanetInfos`.

Como foi dito na seção anterior, a integração com a SWAPI não é estritamente necessária para o funcionamento da API. Caso a SWAPI esteja fora do ar e os dados não possam ser requisitados, a contagem de filmes poderá estar desatualizada na API. Entretanto, sempre que o script de busca é executado, caso alguma inconsistência seja encontrada entre as aparições em filmes do SWAPI e as que foram registradas nos documentos já existentes, o próprio script realizará uma atualização desses dados diretamente. Dessa forma, as inconsistências dos dados, caso existam, podem ser resolvidas a qualquer momento assim que o serviço externo estiver disponível.

## Testes

Para os testes, foi utilizada a biblioteca padrão do Go, em conjunto com os assertions da biblioteca [Testify](https://github.com/stretchr/testify). Para executar os testes, é necessário inicialmente parar a execução do projeto via docker compose (`docker-compose down -v`), para evitar conflito de uso de portas entre o MongoDB do projeto e do ambiente de testes. Os testes então, podem ser executados pelo script `local_test.sh`. Este script realiza diversas ações que foram agrupadas para facilitar a configuração e execução do ambiente:

```
1. Inicializa uma instância de teste do MongoDB via docker
2. Gera um novo arquivo .env a partir do .env.example padrão (para eliminar possíveis problemas de alteração manual)
3. Altera o arquivo .env para conter a connection string da instância de testes
4. Executa os testes passando explicitamente as variáveis de ambiente do arquivo .env (pode ser visto no script run_test.sh)
5. Gera um relatório de coverage dos testes
6. Desfaz a mudança no arquivo .env feita no passo 3
7. Interrompe a execução da instância de teste do MongoDB
```

Foram construídos testes unitários (que se encontram no mesmo package onde as funcionalidades estão implementadas) e testes de integração que possuem um package próprio em `api/integrationtest`.

Como pode ser visto no passo 5, um relatório de coverage é gerado após a execução dos testes. São gerados dois arquivos `coverage.out` e `coverage.html`. O arquivo HTML pode ser aberto no navegador para ter uma visualização mais clara do coverage de cada arquivo do projeto. Foram exclúidos do coverage os arquivos executáveis do diretório `cmd`, o próprio diretório de testes de integração `api/integrationtest` e o script que realiza conexão com o serviço externo da SWAPI.

Como passo opcional, caso tenha instalado a ferramenta [GoCov](https://github.com/axw/gocov) que foi listada nos requisitos, é possível executar o script `consolidate_coverage.sh` para obter um valor consolidado do code coverage do projeto. Executando este script, obtemos o resultado de 94.97% de coverage (considerando os diretórios acima excluídos).

Foi criado também um arquivo de configuração do GitLab CI (ver `.gitlab-ci.yml`), que executa automaticamente os testes, gera o coverage consolidado e salva todos os artefatos gerados pela operação para visualização futura por um período determinado de tempo (configurado como 30 dias).

## Extras

O tutorial de execução utiliza o Docker em conjunto com Docker Compose para a execução da maior parte do projeto. Mas é possível também executar o projeto localmente, utilizando um MongoDB próprio. Entretanto, a configuração do ambiente fica a cargo do usuário. Inicialmente, é necessário configurar o arquivo `.env` corretamente. Abaixo pode ser vista uma descrição dos campos:

```
CONNECTION_STRING # String de conexão com o MongoDB
DATABASE_NAME # Nome do banco de dados dentro do MongoDB
LIST_PAGE_SIZE # Tamanho da página que será retornada na paginação
PLANET_SEED_PATH # Arquivo JSON que contém os dados que podem ser utilizados como substituto da busca na SWAPI
```

Para executar a API, após ter realizado a configuração do seu ambiente e instalado todas as dependências necessárias, pode ser executada com

```
go run cmd/server/main.go
```

O script de busca também pode ser executado. Ele possui dois modos de uso (configurados pela flag `-mode`)

```
go run cmd/swapiworker/main.go
ou
go run cmd/swapiworker/main.go -mode api
```

Este modo é o modo padrão, que realiza a busca dos dados da SWAPI diretamente. Caso a SWAPI esteja fora do ar, ou seja necessário adicionar dados manualmente, é possível criar um arquivo JSON no formato encontrado no diretório `data/planetseed.json`, configurar a variável de ambiente PLANET_SEED_PATH apontando para o arquivo, e executar o comando

```
go run cmd/swapiworker/main.go -mode file
```

Dessa forma, o script, ao invés de buscar os dados da SWAPI, fará a leitura dos dados do arquivo JSON do ambiente local.
