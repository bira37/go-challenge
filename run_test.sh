export $(cat .env | xargs) && go test -coverpkg ./api/errs,./api/planet,./api/server,./api/util,./pkg/... -coverprofile coverage.out ./...

go tool cover -html=coverage.out -o coverage.html
  