echo "Initializing MongoDB..."
cp .env.example .env
sed -i 's/CONNECTION_STRING=mongodb:\/\/user:pass@mongo:27017/CONNECTION_STRING=mongodb:\/\/localhost:27017/' .env
docker run -d --rm --name mongo_test -p 27017:27017 mongo:4.4.1
echo "Running"
bash run_test.sh
sed -i 's/CONNECTION_STRING=mongodb:\/\/localhost:27017/CONNECTION_STRING=mongodb:\/\/user:pass@mongo:27017/' .env
echo "Finished. Stopping MongoDB..."
docker stop mongo_test