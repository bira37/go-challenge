module gitlab.com/bira37/go-challenge

go 1.16

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/labstack/echo/v4 v4.3.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	go.mongodb.org/mongo-driver v1.5.2 // indirect
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf // indirect
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	golang.org/x/sys v0.0.0-20210511113859-b0526f3d8744 // indirect
)
